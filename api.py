from flask import Blueprint, request
from flask_cors import CORS
import logging

api = Blueprint('api_product', __name__)
CORS(api)

from .models import Product, db
from sqlalchemy import desc

@api.route('/all')
def getAllUsers():
    try:
        return Product.returnAll()
    except:
        return {'msg': 'Something went wrong.'}, 500

@api.route('/add', methods=['POST'])
def addProduct():
    data = request.get_json()
    print(request, flush=True)
    try:
        product=Product.createNew(data)
        db.session.add(product)
        db.session.commit()

        return {'msg': 'Product added successfully.'}, 200
    except:
        return {'msg': 'Something went wrong.'}, 500

@api.route('/delete/<pcode>', methods=['DELETE'])
def deleteProduct(pcode):
    try:
        product=Product.query.get(pcode)
        db.session.delete(product)
        db.session.commit()

        return {'msg': 'Product deleted successfully.'}, 200
    except:
        return {'msg': 'Something went wrong.'}, 500


@api.route('/update/<pcode>', methods=['PATCH'])
def updateProduct(pcode):
    if not pcode.isdigit():
        return {'msg': 'Product code not a positive integer.'}, 400
    try:
        data = request.json
        product = Product.query.get(pcode)
        product.pname=data.get('pname', product.pname)
        product.pdescription=data.get('pdescription', product.pdescription)
        product.pprice=data.get('pprice', product.pprice)
        db.session.commit()
        return {'msg': 'Patient updated successfully.'}, 200
    except:
        return {'msg': 'Something went wrong.'}, 500


def init_app(app, url_prefix='/api/v1'):
    app.register_blueprint(api, url_prefix=url_prefix + '/product')


#Comandos utiles 
#curl -i -H "Content-Type: application/json" -X POST -d '{"pcode":2,"pdescription":"4 GB, I3-U3210i, 128GB","pname":"Computadora HP","pprice":250000.0}' http://192.168.100.115:5000/api/v1/product/add
#curl -i -H "Content-Type: application/json" -X PATCH -d '{"pcode":1,"pdescription":"4 GB, I3-U3210i, 128GB","pname":"Desktop HP","pprice":250000.0}' http://192.168.100.115:5000/api/v1/product/update/1
#curl -X GET -i 'http://192.168.100.115:5000/api/v1/product/all'
#curl -X DELETE -i 'http://192.168.100.115:5000/api/v1/product/delete/2'