from flask import Flask

from .api import init_app as init_api
from .models import init_app as init_models

app = Flask(__name__)
app.config ['SQLALCHEMY_DATABASE_URI']='mysql+mysqlconnector://{user}:{password}@{server}/{database}'.format(user='root', password='Hola1357.', server='localhost', database='appmovil')

init_models(app)   
init_api(app) 

@app.route("/appmovil")
def heartbeat():
    return "ok"

#if __name__ == "__main__":
#    app.run(host="0.0.0.0")

#flask run --host=0.0.0.0