
import json
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func

db = SQLAlchemy()
# we create the class Book and extend it from the Base Class.
class Product(db.Model):
    __tablename__ = 'products'
    pcode = db.Column(db.Integer, primary_key=True, autoincrement=True)
    pname = db.Column(db.String(255), nullable=False)
    pdescription = db.Column(db.String(255), nullable=False)
    pprice =db.Column(db.Numeric(8, 2), nullable=False)

    @property
    def serialize(self):
        return {
            'pcode': self.pprice,
            'pname': self.pname,
            'pdescription': self.pdescription,
            'pprice': self.pprice,
        }
    @staticmethod
    def createNew(data):
        return Product(pcode=data['pcode'],pname=data['pname'],pdescription=data['pdescription'],pprice=data['pprice'] )

    @classmethod
    def returnAll(cls):
        def toJson(x):
            return {
                'pcode': x.pcode,
                'pname': x.pname,
                'pdescription': x.pdescription,
                'pprice': float(x.pprice)
            }
        return {"products":list(map(lambda x: toJson(x), Product.query.all()))}

def init_app(app):
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    with app.app_context():
        db.create_all()   









